#!/usr/bin/python

import urllib
import re 

class SearchEngine:
    class SearchURLopener(urllib.URLopener):
        version='Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:43.0) Gecko/20100101 Firefox/43.0'

    def search(self, term):
        search_path = self.search_prefix + term;
        opener = SearchEngine.SearchURLopener()
        r = opener.open(search_path)
        txt = r.read()
        return txt


class Google(SearchEngine):
    def __init__(self):
        self.search_prefix = 'https://www.google.com/search?q='


class Yahoo(SearchEngine):
    def __init__(self):
        self.search_prefix = 'https://search.yahoo.com/search?p='


class Bing(SearchEngine):
    def __init__(self):
        self.search_prefix = 'http://www.bing.com/search?q='


class UserInterface:
    def get_input(self, prompt):
        user_input = raw_input(prompt)
        return user_input

    def display_output(self, text):
        print(text)


class ApplicationController:
    def __init__(self, user_interface):
        self.user_interface = user_interface;
        self.search_term = ''

    def ask_for_search_term(self):
        self.search_term = self.user_interface.get_input("What term would you like to search for? ")

    def display_confirmation(self):
        self.user_interface.display_output("Searching for: '" + self.search_term + "' ...")

    def get_results(self, search_engine):
        txt_response = search_engine.search(self.search_term)
        pattern = re.compile(self.search_term, re.IGNORECASE)
        matches = re.findall(pattern, txt_response)
        return len(matches)

    def display_results(self, google, yahoo, bing):
        self.user_interface.display_output("Google: {} | Yahoo: {} | Bing: {}".format(google, yahoo, bing))


class Main:

    def __init__(self):
        user_interface = UserInterface();
        self.app = ApplicationController(user_interface)
        self.google = Google()
        self.yahoo = Yahoo()
        self.bing = Bing()

    def run(self):
        self.app.ask_for_search_term()
        self.app.display_confirmation()
        google_count = self.app.get_results(self.google)
        yahoo_count = self.app.get_results(self.yahoo)
        bing_count = self.app.get_results(self.bing)
        self.app.display_results(google_count, yahoo_count, bing_count)
        

if __name__ == '__main__':
    main = Main()
    main.run()

