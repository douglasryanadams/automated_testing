#!/usr/bin/python

import unittest # https://docs.python.org/2/library/unittest.html

import code # Code that we'll be testing

class FakeUserInterface:
    def __init__(self):
        self.return_user_input = ''
        self.actual_display_output = ''

    def get_input(self, prompt):
        self.actual_display_output = prompt
        return self.return_user_input

    def display_output(self, text):
        self.actual_display_output = text

class FakeSearchEngine:
    def __init__(self):
        self.search_response = ''

    def search(self, term):
        return self.search_response


class TestWebProject(unittest.TestCase):

    def setUp(self):
        self.fake_user_interface = FakeUserInterface()
        self.fake_search_engine = FakeSearchEngine()
        self.application_controller = code.ApplicationController(self.fake_user_interface)

    def test_hello_worl_unit_test(self):
        self.assertEqual('TEST', 'test'.upper())
        self.assertNotEqual('TeST', 'test'.upper())
    
    def test_application_asks_for_search_term(self): # 1st Test
        self.application_controller.ask_for_search_term()

        self.assertIn('term', self.fake_user_interface.actual_display_output)
        self.assertIn('search', self.fake_user_interface.actual_display_output)
        self.assertIn('?', self.fake_user_interface.actual_display_output)

    def test_application_prints_the_correct_search_term_in_confirmation(self): # 2nd Test
        self.fake_user_interface.return_user_input = 'calendar'
        self.application_controller.ask_for_search_term()
        self.application_controller.display_confirmation()

        self.assertIn('calendar', self.fake_user_interface.actual_display_output)

    def test_application_gets_results_count_correctly(self): # 3rd Test
        self.fake_user_interface.return_user_input = 'calendar'
        self.fake_search_engine.search_response = '''
            calendar blah blah blah calendar
            blah blah calendar clndr blah blah
            blargh calendar blah
            '''

        self.application_controller.ask_for_search_term()
        results = self.application_controller.get_results(self.fake_search_engine)

        self.assertEqual(4, results)

    def test_application_displays_final_results_correctly(self): # 4th Test
        self.application_controller.display_results(9,24,86)
        self.assertIn('Google: 9', self.fake_user_interface.actual_display_output)
        self.assertIn('Yahoo: 24', self.fake_user_interface.actual_display_output)
        self.assertIn('Bing: 86', self.fake_user_interface.actual_display_output)

if __name__ == '__main__':
    unittest.main()
