#!/usr/bin/python

import unittest # https://docs.python.org/2/library/unittest.html

import code # Code that we'll be testing

class FakeUserInterface:
    '''
    Here I'm creating a fake UserInterface because I don't my
    tests to rely on the existence of an interactive terminal.
    '''
    def __init__(self):
        self.return_user_input = ''
        self.actual_display_output = ''

    def get_input(self, prompt):
        self.actual_display_output = prompt
        return self.return_user_input

    def display_output(self, text):
        self.actual_display_output = text

class FakeSearchEngine:
    '''
    As with FakeUserInterface, I don't want to have to actually
    perform a search to make sure that my ApplicationController
    is doing its job correctly, I'll test the SearchEngine
    in another test somewhere else.
    '''

    def __init__(self):
        self.search_response = ''

    def search(self, term):
        return self.search_response


class TestWebProject(unittest.TestCase):

    def setUp(self):
        self.fake_user_interface = FakeUserInterface()
        self.fake_search_engine = FakeSearchEngine()
        self.application_controller = code.ApplicationController(self.fake_user_interface)

    def test_hello_worl_unit_test(self):
        '''
        If you're new to unit testing, welcome! Here's a quick example of a
        very simple python unit test.
        '''
        self.assertEqual('TEST', 'test'.upper())
        self.assertNotEqual('TeST', 'test'.upper())
    
    def test_application_asks_for_search_term(self): # 1st Test
        '''
        What I want to test here: 
            - Essential keywords are displayed in the request prompt.
        What I am NOT testing:
            - The complete sentence used for the prompt
            - What displays the question to the user
            - raw_input()
            - print()
        '''
        self.application_controller.ask_for_search_term()

        # Make sure the word 'term' was used in the prompt
        self.assertIn('term', self.fake_user_interface.actual_display_output)
        # Make sure the word 'search' was also used
        self.assertIn('search', self.fake_user_interface.actual_display_output)
        # Make sure there's a question mark
        self.assertIn('?', self.fake_user_interface.actual_display_output)

    def test_application_prints_the_correct_search_term_in_confirmation(self): # 2nd Test
        '''
        What I want to test here:
            - My search term is repeated in the confirmation
            - Input correctly saved after asking for the term
        What I am NOT testing:
            - Other language in the confirmation
        '''
        self.fake_user_interface.return_user_input = 'calendar'
        self.application_controller.ask_for_search_term()
        self.application_controller.display_confirmation()

        self.assertIn('calendar', self.fake_user_interface.actual_display_output)

    def test_application_gets_results_count_correctly(self): # 3rd Test
        '''
        What I want to test here:
            - My business logic correctly parses and counts keywords in a string
        What I am NOT testing:
            - urllib
            - Internet connections
        '''
        self.fake_user_interface.return_user_input = 'calendar'
        self.fake_search_engine.search_response = '''
            calendar blah blah blah calendar
            blah blah calendar clndr blah blah
            blargh calendar blah
            '''

        self.application_controller.ask_for_search_term()
        results = self.application_controller.get_results(self.fake_search_engine)

        self.assertEqual(4, results)

    def test_application_displays_final_results_correctly(self): # 4th Test
        '''
        What I want to test here:
            - Users get results displayed in a specific format
        What I am NOT testing:
            - Order of results
        '''
        self.application_controller.display_results(9,24,86)
        self.assertIn('Google: 9', self.fake_user_interface.actual_display_output)
        self.assertIn('Yahoo: 24', self.fake_user_interface.actual_display_output)
        self.assertIn('Bing: 86', self.fake_user_interface.actual_display_output)

if __name__ == '__main__':
    unittest.main()
