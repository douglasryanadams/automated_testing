#!/usr/bin/python

import urllib # https://docs.python.org/2/library/urllib.html
import re # https://docs.python.org/2/library/re.html



class SearchEngine:
    '''
    This class provides an abstraction layer between the application and the
    actual HTTP connection negotiation required for executing the search.

    As with UserInterface, by abstracting this implementation away from the
    business logic of our application we make it easier to test our business
    logic and easier to change our implementation for executing the actual
    HTTP requests.
    '''
    class SearchURLopener(urllib.URLopener):
        '''
        This sets the User Agent string for my requests so that my client looks more like a browser
        '''
        version='Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:43.0) Gecko/20100101 Firefox/43.0'

    def search(self, term):
        '''
        Executes the search for the term using the search prefix defined
        in the sub class.
        '''
        search_path = self.search_prefix + term;
        opener = SearchEngine.SearchURLopener()
        r = opener.open(search_path)
        txt = r.read()
        return txt


class Google(SearchEngine):
    def __init__(self):
        self.search_prefix = 'https://www.google.com/search?q='


class Yahoo(SearchEngine):
    def __init__(self):
        self.search_prefix = 'https://search.yahoo.com/search?p='


class Bing(SearchEngine):
    def __init__(self):
        self.search_prefix = 'http://www.bing.com/search?q='


class UserInterface:
    '''

    This class provides an abstraction layer between the application and the
    presentation of information to the user. By abstracting this feature we
    could swap out the interface for something else (like a web form) in the
    future. Abstracting this functionality out will also make it easier for
    us to test our code because we'll have the opportunity to write our own
    'fake' interface and interact with specific components of the application.

    So how do we test this component itself you may ask? Easy, just execute 
    the script. Presentation is the easiest component for a human to detect
    issues with. If 'display_output' fails, you (as a human) will notice
    immediately.

    raw_input doc: https://docs.python.org/2/library/functions.html#raw_input

    '''

    def get_input(self, prompt):
        '''
        Retrieves user input from the command line
        '''
        user_input = raw_input(prompt)
        return user_input

    def display_output(self, text):
        '''
        Displays text to the command line
        '''
        print(text)


class ApplicationController:
    '''

    This class stands in for a component in a larger application where the Main
    class would be kicking off and initializing several factories or controllers
    to be used during the lifecycle of the application.

    I've written this class separately here for demonstration purposes.

    '''
    def __init__(self, user_interface):
        self.user_interface = user_interface;
        self.search_term = ''

    def ask_for_search_term(self):
        self.search_term = self.user_interface.get_input("What term would you like to search for? ")

    def display_confirmation(self):
        self.user_interface.display_output("Searching for: '" + self.search_term + "' ...")

    def get_results(self, search_engine):
        txt_response = search_engine.search(self.search_term) # Go do the actual search
        pattern = re.compile(self.search_term, re.IGNORECASE) # Create a pattern to use for finding the search terms
        matches = re.findall(pattern, txt_response) # Go find all the words that match the pattern
        return len(matches) # Return a count of the matches found

    def display_results(self, google, yahoo, bing):
        self.user_interface.display_output("Google: {} | Yahoo: {} | Bing: {}".format(google, yahoo, bing))


class Main:

    def __init__(self):
        user_interface = UserInterface();
        self.app = ApplicationController(user_interface)
        self.google = Google()
        self.yahoo = Yahoo()
        self.bing = Bing()

    def run(self):
        '''
        Note how easy it is to get a high level understanding of the order of
        operations for the logic of this application in the lines below.

        By properly abstracting our components, we not only make them easier 
        to test but also make our code easier to read and follow.
        '''
        self.app.ask_for_search_term()
        self.app.display_confirmation()
        google_count = self.app.get_results(self.google)
        yahoo_count = self.app.get_results(self.yahoo)
        bing_count = self.app.get_results(self.bing)
        self.app.display_results(google_count, yahoo_count, bing_count)
        

if __name__ == '__main__':
    main = Main()
    main.run()

