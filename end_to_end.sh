#!/bin/bash

# Execute the script (code.py) and provide the response 'toaster' when it asks
# for a search term.
output=$(expect <<- DONE
    spawn ./code.py
    expect "*?*"
    send -- "toaster\r"
    expect eof
DONE)

# Check the output to make sure we got a well formatted response back
echo "$output" | grep -E "Google: \d+ \| Yahoo: \d+ \| Bing: \d+" > /dev/null 2>&1

# If we found the correct response:
if [ $? -eq 0 ]
then
    # Pass the test
    echo "PASS"
else
    # Otherwise, fail the test
    echo "FAIL"
fi
